#Author: Giacomo Vianello (giacomov@stanford.edu)

class Counter(object):
  '''
This class allow to count how many possible combinations of changes can be
found for a given amount of money, given a set of coin denominations
  '''
  def __init__(self,denominations=[25,10,5,1]):
    
    try:
      #Using set() assure that the list has only unique elements
      self.denominations         = list(set(denominations))
    except TypeError:
      print("You have to provide an iterable (like a list or tuple) as denominations")
      raise
    else:
      #Make sure the list is ordered in descending order
      self.denominations         = sorted(self.denominations)[::-1]
      
      self.lookupTable           = {}
    pass
  pass
    
  def count(self,amount):
    '''
    This uses an iterative approach to count the number of combinations.
    
    Usage: count(amount)
    
    Parameters:
          amount                - Total amount of money.    
    Returns:
          The number of ways in which it is possible to split the given amount using
          coins of the provided denominations 
    '''
    
    #The first step is to rescale everything so that the minimum available
    #denomination is equal to 1, because the following algorithm treat only
    #amounts which are integer multiple of the minimum available denomination.
    #
    #This pre-step allow to handle situations where we
    #want to keep the amount in "natural units". For example, an amount of 
    #1 dollar can be divided using the US denominations of 1 cent, 5 cent,
    #25 cent, 50 cent, 1 dollar and so on, like this:
    #c = Counter.Counter([0.01,0.05,0.1,0.25,0.5,1,5,10,20,50,100])
    #c.count(1)
    #> 293
    minDenomination             = min(self.denominations)
    
    if(minDenomination != 1):
      #Transform the denominations and the amount, scaling them so that
      #the minimum denomination is equal to 1. This allow to handle
      #floating point amounts
      renorm                    = 1.0/minDenomination
      cAmount                   = int(amount*renorm)
      cDenominations            = map(lambda x:int(x*renorm),self.denominations)
    else:
      cAmount                   = int(amount)
      cDenominations            = self.denominations
    pass
    
    #If the rescaled amount is not a multiple of the minimum available denomination,
    #then there is no possible split solution    
    if(cAmount % min(cDenominations)!=0):
      return 0
    
    n                           = int(cAmount)
    
    #Number of denominations
    m                           = len(cDenominations)
    
    for i in xrange(cAmount+1):
      
      #here we are considering the case for which
      #we want to compute the number of partitions
      #for subtotal i
      
      for j in xrange(-1,m):
        
        #Considering denomination j 
        #(remember, j=0 is the largest denomination)
        #The cycle with j==-1 is a trick to condense everything
        #in one loop
        
        if(i==0):
          #There is only one way of partitioning 0,
          #which is with 0 coins, independently of the
          #denomination under consideration
          self.lookupTable[i,j] = 1
          
        elif(j==-1):
          #No change available
          self.lookupTable[i,j] = 0
        
        elif(i < cDenominations[j]):
          #If the denomination under consideration is larger than the current
          #subtotal, the number of combinations is equal to the number of combinations
          #without this denomination (and without all the denominations larger 
          #than the current one), which we already computed
          self.lookupTable[i,j] = self.lookupTable[i,j-1]
        
        else:
          #The total number of combinations is equal to the 
          #(number of combinations without the current denomination)
          #+ (the number of combinations with only the current denomination)
          self.lookupTable[i,j] = (self.lookupTable[i - cDenominations[j],j] + 
                                  self.lookupTable[i, j-1])
        pass
      pass
    
    pass
    return self.lookupTable[cAmount,m-1]
  pass
    
pass
