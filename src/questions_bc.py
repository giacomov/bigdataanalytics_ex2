'''
This script answer questions b and c of the exercise 2 of
the Big Data Analytics school at JPL/Caltech
'''

import Counter

c = Counter.Counter()

tot = 0
for i in range(1,101):
  thisCount                   = c.count(i)
  if(thisCount % 2!=0):
    #This is not even
    tot                      += thisCount
  pass
pass

print("Answer to question b is %s" %(tot))

oddOrEven                     = 0
if(tot %2==0):
  oddOrEven                   = 'even'
else:
  oddOrEven                   = 'odd'

print("Answer to question c is %s" %(oddOrEven))
