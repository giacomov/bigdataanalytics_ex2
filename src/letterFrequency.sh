#!/bin/bash

dictionary='/usr/share/dict/american'
tempfile='__letterTemp.txt'
rm -rf $tempfile

for letter in {a..z}; do
  occurencies=$(fgrep -i -o $letter $dictionary | wc -l 2>&1)
  echo "Letter $letter occur $occurencies times"
  echo "$letter $occurencies" >> $tempfile
done

echo "Letters in decreasing order of frequency:"
sort $tempfile -r -k 2 -n | gawk '{print $1}' | tr -d "\n"
echo ""

rm -rf $tempfile
