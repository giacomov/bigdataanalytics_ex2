#Author: Giacomo Vianello (giacomov@stanford.edu)

import unittest
import Counter
import sys

class TestChangeFunction(unittest.TestCase):
    '''
Test the count() method of the Counter class against some known cases
    '''
    def setUp(self):
        self.denominations = [[100, 50, 25, 10, 5, 1],
                              [200, 100, 50, 20, 10, 5, 2, 1],
                              [0.01,0.05,0.1,0.25,0.5,1,5,10,20,50,100]] #US dollars
        self.knownCases = [[100,100000,1000000],[100,100000,1000000],[1,10,100]]
        self.knownResults = [
                             [293,13398445413854501,
                              1333983445341383545001],
                              [4563,10056050940818192726001,
                              99341140660285639188927260001],
                              [293,2202009,
                              1424039612939]
                              ]

    def test_change(self):
        sys.stderr.write("Testing count() method of Counter class:\n")
        #Test the change function with some known cases
        testN                 = 1
        for i,denomination in enumerate(self.denominations):
          
          thisCounter         = Counter.Counter(denomination)
          
          for testCase,result in zip(self.knownCases[i],self.knownResults[i]):
            sys.stderr.write("Subtest %s..." %(testN))
            self.assertEqual(thisCounter.count(testCase),result)
            sys.stderr.write("passed\n")
            testN            += 1
            
if __name__ == '__main__':
    unittest.main()
